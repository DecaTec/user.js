/******************************************************************************
 * user.js                                                                    *
 * https://codeberg.org/DecaTec/user.js                                       *
 ******************************************************************************/

user_pref("browser.urlbar.clickSelectsAll", true);

// Enable first-party isolation
// https://wiki.mozilla.org/Security/FirstPartyIsolation
user_pref("privacy.firstparty.isolate", true);

// Disable upload of screenshots
user_pref("extensions.screenshots.upload-disabled", true);

// Disable Pocket
// https://support.mozilla.org/en-US/kb/save-web-pages-later-pocket-firefox
user_pref("extensions.pocket.enabled", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);

// Disable Mozilla telemetry/experiments
// https://wiki.mozilla.org/Platform/Features/Telemetry
// https://wiki.mozilla.org/Privacy/Reviews/Telemetry
// https://wiki.mozilla.org/Telemetry
// https://www.mozilla.org/en-US/legal/privacy/firefox.html#telemetry
// https://wiki.mozilla.org/Telemetry/Experiments
user_pref("toolkit.telemetry.enabled", false);
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("experiments.supported", false);
user_pref("experiments.enabled", false);
user_pref("experiments.manifest.uri", "");