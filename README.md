# Custom user.js for Firefox

## Installation

### Single profile installation

Copy/download `user.js` to your current user profile directory.
To find out the current user profile directory, open *Help* > *Troubleshooting information*. You can also simply call `about:support` Here you can find the property *Profile Directory*.

Paths:

| OS                         | Path                                                                                                                                          |
| -------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| Windows                    | `%APPDATA%\Mozilla\Firefox\Profiles\XXXXXXXX.your_profile_name\user.js`                                                                       |
| Linux                      | `~/.mozilla/firefox/XXXXXXXX.your_profile_name/user.js`                                                                                       |
| OS X                       | `~/Library/Application Support/Firefox/Profiles/XXXXXXXX.your_profile_name`                                                                   |

### Verification

When calling `about:support`, there should be a section indicating that a custom user.js was loaded:

![user.js in about:support](https://codeberg.org/DecaTec/user.js/raw/branch/master/img/about_support_user.js.png)